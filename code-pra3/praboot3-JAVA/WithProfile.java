/**
 * WithProfile
 */
public interface WithProfile {

    public abstract String getName();
    public abstract void setName(String name);

}