import java.util.Scanner;

/**
 * Main
 */
public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("What's your name? ");
        String name = scanner.nextLine();
        Player player = new Player(name);
        player.interact();
        Pokemon charmander = new Pokemon("Charmander", 25, 15, 10);
        charmander.animate();
        charmander.interact();

        scanner.close();
    }
}