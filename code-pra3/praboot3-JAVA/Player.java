public class Player implements WithProfile, Interactable{
    private String name;

    Player(String name){
        this.setName(name);
    }
    

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void interact() {
        System.err.println("Hello, my name is " + this.getName());
    }
}
