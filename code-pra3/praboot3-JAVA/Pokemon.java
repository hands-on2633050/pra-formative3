public class Pokemon implements WithAttributes, WithProfile, Interactable {
    
    private int strength = 0;
    private int speed = 0;
    private int defense = 0;

    private String name;

    Pokemon(String name, int strength, int speed, int defense){
        this.setName(name);
        this.setup(strength, speed, defense);
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

   @Override
   public void setStrength(int points) {
       this.strength = points;
   }

   @Override
   public void setSpeed(int points) {
       this.speed = points;
   }

   @Override
   public void setDefense(int points) {
       this.defense = points;
   }

   @Override
   public int getStrength() {
       return this.strength;
   }

   @Override
   public int getSpeed() {
       return this.speed;
   }

   @Override
   public int getDefense() {
       return this.defense;
   }

   @Override
   public boolean setup(int strength, int speed, int defense) {
    if((strength + speed + defense) == MAX_STATUS_POINTS) {
        this.setStrength(strength);
        this.setSpeed(speed);
        this.setDefense(defense);
        return true;
    }else {
        System.out.println("Failed setup. Points must add up to " + MAX_STATUS_POINTS + "!");
        return false;
    }
   }

   @Override
   public void animate() {
    System.out.println("Displaying " + this.getName() + "...");
    System.out.print("Strength: \t");
    loadAttribute('X', this.getStrength());
    System.out.print("Defense: \t");
    loadAttribute('#', this.getDefense());
    System.out.print("Speed: \t\t");
    loadAttribute('>', this.getSpeed());

       
   }

   public void loadAttribute(char symbol, int points){
    try {
        for(int i = 0; i < points; i++){
            System.out.print(symbol);
            Thread.sleep(100);
        
        }
        System.out.print("\n");
    } catch (InterruptedException e) {
        System.out.println("Something went wrong!");
    }
   }

   @Override
   public void interact() {
       System.out.println("Meep-meep-meep");;
   }

}

