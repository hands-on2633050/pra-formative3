/**
 * WithAttributes
 */
public interface WithAttributes {

    final int MAX_STATUS_POINTS = 50;

    public abstract void setStrength(int points);
    public abstract void setSpeed(int points);
    public abstract void setDefense(int points);

    public abstract int getStrength();
    public abstract int getSpeed();
    public abstract int getDefense();

    public abstract boolean setup(int strength, int speed, int defense);

    public abstract void animate();


}